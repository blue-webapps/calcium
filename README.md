# calcium

## live development

in this mode the webapp is served by webpack-dev-server, when you make a change your browser will automatically reload to show the change

```bash
cd ~/projects/mywebapp
npm run start
```

open <http://localhost:8080/> in your browser

## build release

in this mode webpack is used to bundle your webapp into the dist directory ready to be deployed to your server, it is uglified and minified with sourcemaps, if your webapp works offline you can zip the dist folder and send it to someone and they can unzip and run your app by opening index.html

```bash
cd ~/projects/mywebapp
npm run build
```

# formatting & linting

i have included javascript formatting and linting tools in the form of js-beautify and eslint, both configured to fix automatically where possible. you can edit the configs at `.jsbeautifyrc` and `.eslintrc.json`

```bash
npm run lint
```
