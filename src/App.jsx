/* eslint react/no-unused-state:off */

import React from 'react';
import { Grid, Segment } from 'semantic-ui-react';
import Title from './components/Title';
import Month from './components/Month';
import Toolbar from './components/Toolbar';
import data from './lib/data';
import { YEAR } from './lib/convert';

export default class Main extends React.Component {
  constructor() {
    super();
    this.state = { trigger: true };

    let timeout;
    const redraw = () => {
      clearTimeout(timeout);
      timeout = setTimeout(() => this.setState({ trigger: true }), 50);
    };
    window.addEventListener('resize', redraw);
    window.addEventListener('hashchange', redraw, false);
    data.on('update', redraw);
  }

  render() {
    const days = data.get();
    let key = 0;

    console.log(days);

    const months = days.map(v => (
      <Grid.Column key={key++}>
        <Month days={v} />
      </Grid.Column>
    ));
    const cols = window.innerWidth < 600 ? 1 : window.innerWidth > 3600 ? 6 : Math.round(window.innerWidth / 600);
    return (
      <Segment basic>
        <Title title={`${YEAR}`} />
        <Grid columns={cols} stackable>
          {months}
        </Grid>
        <Toolbar />
      </Segment>
    );
  }
}
