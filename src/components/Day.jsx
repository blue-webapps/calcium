import React from 'react';
import { Grid } from 'semantic-ui-react';
import { mouseDowned, mouseMoved } from '../lib/mouse';

export default class Day extends React.Component {
  render() {
    const { day } = this.props;
    const color = day.today ? (day.selected ? 'teal' : 'green') : day.selected ? 'blue' : undefined;
    return (
      <Grid.Column
        onMouseDown={() => mouseDowned(day)}
        onMouseMove={() => mouseMoved(day)}
        color={color}
        style={{ cursor: 'default', userSelect: 'none' }}
      >
        {day.getDate()}
      </Grid.Column>
    );
  }
}
