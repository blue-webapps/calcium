import React from 'react';
import { Grid, Header, Segment } from 'semantic-ui-react';
import Day from './Day';
import DayNames from './DayNames';

const dayNames = <DayNames />;

export default class Month extends React.Component {
  render() {
    let key = 0;
    const { days } = this.props;
    const name = days[0].toLocaleDateString(undefined, { month: 'long' });

    const list = [];

    let cRow = [];
    Array(days[0].getDay())
      .fill()
      .forEach(() => cRow.push(<Grid.Column key={key++} />));

    days.forEach(i => {
      cRow.push(<Day day={i} key={key++} />);
      if (cRow.length === 7) {
        list.push(<Grid.Row key={key++}>{cRow}</Grid.Row>);
        cRow = [];
      }
    });
    if (cRow.length !== 0) {
      while (cRow.length !== 7) {
        cRow.push(<Grid.Column key={key++} />);
      }
      list.push(<Grid.Row key={key++}>{cRow}</Grid.Row>);
    }

    return (
      <Segment textAlign="center">
        <Header>{name}</Header>
        <Grid columns={7}>
          {dayNames}
          {list}
        </Grid>
      </Segment>
    );
  }
}
