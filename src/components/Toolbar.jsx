import React from 'react';
import { Button, Label, Segment } from 'semantic-ui-react';
import selected from '../lib/selected';

export default class Toolbar extends React.Component {
  render() {
    return (
      <Segment style={{ position: 'fixed', top: 0, left: -2 }}>
        <Label circular size="big" color="green" content={selected.count()} />
        <Button circular color="blue" icon="copy" onClick={() => selected.copy()} />
        <Button circular color="red" icon="trash" onClick={() => selected.clear()} />
      </Segment>
    );
  }
}
