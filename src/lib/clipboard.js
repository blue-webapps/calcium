const copy = text => {
  const a = document.createElement('textarea');
  a.value = text;
  document.body.appendChild(a);
  a.select();
  document.execCommand('Copy');
  document.body.removeChild(a);
};

module.exports = { copy };
