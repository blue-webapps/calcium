const YEAR = new Date().getFullYear();
const DAY = 1000 * 60 * 60 * 24;
const JAN1 = new Date(YEAR, 0, 0);

const toDate = n => {
  const r = new Date(JAN1.valueOf() + n * DAY);
  return r;
};

const toDayNo = n => {
  const r = Math.round((n - JAN1) / DAY);
  return r;
};

module.exports = { YEAR, toDate, toDayNo };
