const { YEAR, toDayNo } = require('./convert');

const TODAY = toDayNo(new Date().setHours(0));

const genMonth = (month, year) => {
  const a = new Date(year, month, 1);
  const r = [];
  while (a.getMonth() === month) {
    const b = new Date(a);
    b.today = TODAY === toDayNo(b);
    r.push(b);
    a.setDate(a.getDate() + 1);
  }
  return r;
};

const genYear = (year = YEAR) => {
  const r = [];
  for (let i = 0; i < 12; i++) {
    r.push(genMonth(i, year));
  }
  return r;
};

module.exports = { genYear };
