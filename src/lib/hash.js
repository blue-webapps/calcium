const { EventEmitter } = require('events');

const emitter = new EventEmitter();

let lastHash = '';

const get = () =>
  window.location.hash
    .replace('#days=[', '')
    .replace(']', '')
    .split('+')
    .map(i => Number(i))
    .filter(i => i !== 0);

const parse = () => {
  if (lastHash !== window.location.hash) {
    lastHash = window.location.hash;
    emitter.emit('update', get());
  }
};

const update = l => {
  const b = l.join('+');
  window.location.hash = `#days=[${b}]`;
  lastHash = window.location.hash;
};

window.addEventListener('hashchange', () => parse());
parse();
const on = emitter.on.bind(emitter);

module.exports = { update, on, get };
