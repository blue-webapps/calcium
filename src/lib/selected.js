/* eslint no-param-reassign:off */

const { EventEmitter } = require('events');
const { toDate, toDayNo } = require('./convert');
const clipboard = require('./clipboard');
const hash = require('./hash');

const emitter = new EventEmitter();
let list = [];

const onUpdate = () => {
  list.sort();
  hash.update(list);
  emitter.emit('update', list);
};

const add = d => {
  const n = toDayNo(d);
  if (!list.includes(n)) {
    console.log('add', d);
    d.list = true;
    list.push(n);
    list.sort((a, b) => (a > b ? 1 : a < b ? -1 : 0));
    onUpdate();
  }
};

const remove = d => {
  const n = toDayNo(d);
  if (list.includes(n)) {
    console.log('remove', d);
    d.list = false;
    list = list.filter(i => i !== n);
    onUpdate();
  }
};

const clear = () => {
  list = [];
  onUpdate();
};

const copy = () => {
  const a = list
    .map(i =>
      toDate(i).toLocaleDateString(undefined, {
        weekday: 'short',
        year: 'numeric',
        month: 'short',
        day: 'numeric'
      })
    )
    .join('\r\n');
  clipboard.copy(a);
};

const get = () => list;

const count = () => list.length;

const on = emitter.on.bind(emitter);

list = hash.get();

hash.on('update', l => {
  list = l;
  onUpdate();
});

module.exports = { on, add, remove, clear, copy, get, count };
